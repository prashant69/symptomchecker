$(document).on("pageinit","#datapage",function(){
	
    console.log("in getdata.js");
	var url = window.location;
    var data="";
	var j;
	 console.log(url);
	
	 // SUBMIT FORM
	$("#dataform").submit(function(event) {
		console.log("in dataform");
		// Prevent the form from submitting via the browser.
		event.preventDefault();
		getData();
	});
	
	function getData(){
		// DO POST
		$.ajax({
			type : "GET",
			contentType : "application/json",
			url : url.origin + "/postsymptom/getData",
			success : function(result) {
				console.log(result);
				if(result.status == "success")
				{
						var slen=result.symresponse.length;
						var flen=result.fresponse.length;
						var dlen=result.dresponse.length;
						data+="<table id='datatable' style='border:1px solid black'><tr><th>Symptoms</th><th>Factors</th>" +
								"<th>Diseases</th></tr>" +
								"<tr><td style='vertical-align:top'><ul id='symptoms'>";
						for(j=0;j<slen;j++){
							data+="<li>" + result.symresponse[j].name + "</li>";
						}
						data+="</ul></td>";
						data+="<td style='vertical-align:top'><ul id='factors'>";
				for(j=0;j<flen;j++){
					data+="<li>" + result.fresponse[j].name + "</li>";
				}
				data+="</ul></td>";
				data+="<td style='vertical-align:top'><ul id='diseases'>";
				for(j=0;j<dlen;j++){
					data+="<li>" + result.dresponse[j].name + "</li>";
				}
				data+="</ul></td></tr></table>";
				$("#dataresult").html(data); 
				}
					else{
						$("#dataresult").html("<p>No data found</p>");
					}
				},
				
				error : function(e) {
					alert("Error!")
					console.log("ERROR: ", e);
				}
			});
		resetData();

		}

		function resetData(){
			//$("#symptom").val("");
			data="";
		}
	
	
})
