$(document).on("pageinit","#addpage",function(){
	
    console.log("in add_data.js");
	var url = window.location;
    var adata="";
	
	 console.log(url);
	
	 // SUBMIT FORM
	$("#addform").submit(function(event) {
		// Prevent the form from submitting via the browser.
		event.preventDefault();
		ajaxAdd();
	});
    

	function ajaxAdd(){

		// PREPARE FORM DATA
		var postData = {
				symname : $("#symtext").val(),
				factname : $("#facttext").val(),
				factrel: $("#factrel").val(),
				inrel: $("#inrel").val(),
				disname: $("#distext").val()
				
		}
	console.log(postData);
		// DO POST
		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : url.origin + "/postsymptom/addData",
			data : JSON.stringify(postData),
			dataType : 'json',
			success : function(result) {
				$("#addform")[0].reset();
			     console.log(result);
				if(result.status == "success"){
					
					var len=result.symresponse.length;
					adata+=result.symresponse[0].name;
					$("#addresult").html("<p>"+adata+" is added to the database</p>");	
					
			}
				else{
					$("#addresult").html("<p>Error while adding data</p>");
				}
			},
			error : function(e) {
				alert("Error!")
				console.log("ERROR: ", e);
			}
			
			});
		resetaddData();
	}

function resetaddData(){
	adata="";
}
	}
	)
			

		
