$(document).on("pageinit","#sympage",function(){
	
	console.log("in sympage");
    

	var url = window.location;
	var ftext="";
	var i;
	var tempdata=[];
	var srchdata=[];
	tempdata=$("#hsearch").val();
	srchdata=tempdata.split(",");
	console.log(srchdata);
	
	 $("#autosearch" ).autocomplete({
		source:srchdata
	    });
	 
	 //Search form
	 $("#searchform").submit(function(event) {
			console.log("in searchform");
			// Prevent the form from submitting via the browser.
			event.preventDefault();
			searchPost();
		});

	// SUBMIT FORM
	$("#symform").submit(function(event) {
		console.log("in symform");
		// Prevent the form from submitting via the browser.
		event.preventDefault();
		ajaxPost();
	});
	
	/*$("#symptomform").submit(function(event) {
		console.log("in symptom form");
		// Prevent the form from submitting via the browser.
		event.preventDefault();
		ajaxPost();
	});*/
    

	function ajaxPost(){

		// PREPARE FORM DATA
		var symData = {
				question : $("#selsymptom").val(),
				uname: $("#huser").val()
		}
      console.log(symData);
		// DO POST
		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : url.origin + "/postsymptom/getFactors",
			data : JSON.stringify(symData),
			dataType : 'json',
			success : function(result) {
				$("#symform")[0].reset();
				console.log(result)
				if(result.status == "success"){
					var len=result.fresponse.length;
					if(len>0){
						ftext+="<form id='isform' data-ajax='false'>"+
						"<input type='hidden' id='question' value='"+result.symname+"'></input>"+
						"<input type='hidden' id='huser' value='"+result.uname+"'></input>"+
						"<h3>"+result.symname.toUpperCase()+"</h3>"+
						 "<div class='ui-field-contain'><fieldset data-role='controlgroup'>";
							for(i=0;i<len;i++){
				     ftext+="<input type='checkbox' id='"+result.fresponse[i].id+"' value='"+result.fresponse[i].name+"'>"+
							"<label for='"+result.fresponse[i].id+"'>"+result.fresponse[i].inrel +" " + result.fresponse[i].name+"</label>";
							}
							
						ftext+="</fieldset></div><label for='finddis'></label><button type='submit' id='finddis' data-inline='true' data-theme='b'>Find Disease</button></form>";
						ftext+="<div id='back'><a href='#' data-rel='back' id='fback' class ='ui-btn ui-shadow ui-corner-all ui-btn-left ui-btn-inline ui-btn-icon-left ui-icon-back ui-btn-b ui-mini' data-ajax='false'>Back</a></div>";
						ftext+="<div id='flout'><a href='#login' id='flogout' class ='ui-btn ui-shadow ui-corner-all ui-btn-right ui-btn-inline ui-btn-b ui-mini'>Logout</a></div>";
						var FactPage = $("<div data-role='page' data-url='factors' id='factpage' align='center'><div data-role='header'><b>Related Factors</b></div><div data-role='main' class='ui-content'> "+ ftext + "</div></div>");

						//append it to the page container
						FactPage.appendTo( $.mobile.pageContainer );

						//go to it
						$.mobile.changePage( FactPage,{transition:"none"});
				
	                    }
					else{
						$("#sympage").append("<p>no factors found</p>");
					}
			}
			},
			
			error : function(e) {
				alert("Error!")
				console.log("ERROR: ", e);
			}
		});

		// Reset FormData after Posting
		resetFactData();

	}
	
	function searchPost(){

		// PREPARE FORM DATA
		var symData = {
				question : $("#autosearch").val(),
				uname: $("#huser").val()
		}
      console.log(symData);
		// DO POST
		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : url.origin + "/postsymptom/getFactors",
			data : JSON.stringify(symData),
			dataType : 'json',
			success : function(result) {
				$("#symform")[0].reset();
				console.log(result)
				if(result.status == "success"){
					var len=result.fresponse.length;
					if(len>0){
						ftext+="<form id='isform' data-ajax='false'>"+
						"<input type='hidden' id='question' value='"+result.symname+"'></input>"+
						"<input type='hidden' id='huser' value='"+result.uname+"'></input>"+
						"<h3>"+result.symname.toUpperCase()+"</h3>"+
						 "<div class='ui-field-contain'><fieldset data-role='controlgroup'>";
							for(i=0;i<len;i++){
				     ftext+="<input type='checkbox' id='"+result.fresponse[i].name+"' value='"+result.fresponse[i].name+"'>"+
							"<label for='"+result.fresponse[i].name+"'>"+result.fresponse[i].inrel +" " + result.fresponse[i].name+"</label>";
							}
							
						ftext+="</fieldset></div><label for='finddis'></label><button type='submit' id='finddis' data-inline='true' data-theme='b'>Find Disease</button></form>";
						ftext+="<div id='back'><a href='#' data-rel='back' id='fback' class ='ui-btn ui-shadow ui-corner-all ui-btn-left ui-btn-inline ui-btn-icon-left ui-icon-back ui-btn-b ui-mini' data-ajax='false'>Back</a></div>";
						ftext+="<div id='flout'><a href='#login' id='flogout' class ='ui-btn ui-shadow ui-corner-all ui-btn-right ui-btn-inline ui-btn-b ui-mini'>Logout</a></div>";
						var FactPage = $("<div data-role='page' data-url='factors' id='factpage' align='center'><div data-role='header'><b>Related Factors</b></div><div data-role='main' class='ui-content'> "+ ftext + "</div></div>");

						//append it to the page container
						FactPage.appendTo( $.mobile.pageContainer );

						//go to it
						$.mobile.changePage( FactPage,{transition:"none"});
				
	                    }
					else{
						$("#sympage").append("<p>no factors found</p>");
					}
			}
			},
			
			error : function(e) {
				alert("Error!")
				console.log("ERROR: ", e);
			}
		});

		// Reset FormData after Posting
		resetFactData();

	}

	function resetFactData(){
		ftext="";
	}
})


		
