$(document).on("pageinit","#login",function(){
	console.log("in login validator.js");

	var url = window.location;
	
	var stext="";
	var searcharr=[];
	
	console.log(url);
	

	// SUBMIT FORM
	$("#logform").submit(function(event) {
		// Prevent the form from submitting via the browser.
		event.preventDefault();
		ajaxPost();
	});
    

	function ajaxPost(){
         console.log(url);
		// PREPARE FORM DATA
		var logData = {
				name : $("#uname").val(),
				password : $("#upassword").val()
		}
		console.log(logData);
		// DO POST
		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : url + "/login/validate",
			data : JSON.stringify(logData),
			dataType : 'json',
			success : function(result) {
				$("#logform")[0].reset();
				console.log(result);
					if(result.status == "success"){
						
						var len=result.symresponse.length;
						if(len>0){
							for(i=0;i<len;i++){
								searcharr[i]=result.symresponse[i].name;
							}
							console.log(searcharr);
							stext+="<div id='display'><p>Welcome "+result.uname+"</p></div><br/><br/><br/>"+
							"<div id='symdiv' align='center'>"+
						     "<div id='searchdiv' style='float:left'>" +
						     "<form id='searchform'><table><tr><td>"+
						     "<input type='hidden' id='hsearch' value='"+searcharr+"'></input>"+
						     "<div class='ui-widget'><label for='autosearch'><b>Search Symptom:</b></label></td><td><input type='search' id='autosearch'></td></div>"+
								"<td colspan='2'><button type='submit' id='searchbtn' data-inline='true' data-theme='b'>Search</button></td></tr></table></form></div>"+
							 "<div id='seldiv' style='float:right,margin-left:50px'>" +
						     "<form id='symform' data-ajax='false'><table><tr><td>"+
							"<input type='hidden' id='huser' value='"+result.uname+"'></input>"+
							"<label for='selsymptom'><b>Select Symptom :</b></label></td><td> <select id='selsymptom' data-inline='true'>";
							stext+="<option value=''>Select</option>";
							for(i=0;i<len;i++){
							stext+="<option value='"+result.symresponse[i].name+"'>"+result.symresponse[i].name+"</option>";
							}
							stext+="</select></td></tr><tr><td colspan='2'><button type='submit' id='postbtn' data-inline='true' data-theme='b'>Submit</button></td></tr></table>"+
	                               "</form></div></div><br/><br/><br/>";
							stext+="<div id='back'><a href='#login' id='sback' class ='ui-btn ui-shadow ui-corner-all ui-btn-left ui-btn-inline ui-btn-b ui-mini'>Logout</a></div>";
						symPage = $("<div data-role='page' id='sympage' data-url='symptoms' align='center'><div data-role='header'>Symptom Checker</div><div data-role='main' class='ui-content'> "+ stext + "</div></div>");

						//append it to the page container
						symPage.appendTo( $.mobile.pageContainer );
						
						//go to it
						$.mobile.changePage( symPage , {transition:"none"});
						
				}
					
				}
					else{
						$("#logresult").html("<p style='color:red'>Invalid Username or Password</p>");
					}
			},
				error : function(e) {
					alert("Error!")
					console.log("ERROR: ", e);
				}
			});
		resetSymData();
		}
	
	function resetSymData(){
		stext="";
	}
	}
	)