package com.telehealth365.SymptomChecker.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telehealth365.SymptomChecker.domain.Disease;
import com.telehealth365.SymptomChecker.domain.Factor;
import com.telehealth365.SymptomChecker.domain.Symptom;
import com.telehealth365.SymptomChecker.repository.DiseaseRepository;
import com.telehealth365.SymptomChecker.repository.FactRepository;
import com.telehealth365.SymptomChecker.repository.SymptomRepository;

@Service
public class SymptomService {
	@Autowired
	FactRepository<Factor> factrepository;
	
	@Autowired
	SymptomRepository<Symptom> symrepository;
		
	@Autowired
	DiseaseRepository<Disease> disrepository;
	
	public List<Symptom> findConnected(){
		return symrepository.findSymptoms();
	}
	
	public List<Factor> findFactors(){
		return factrepository.findAllFactors();
	}
	
	public List<Disease> findDiseases(){
		return disrepository.findAllDiseases();
	}

	public List<Factor> getRelated(String name){
		return factrepository.findRelated(name);
	}
    
	public List<Symptom> getRelatedSyms(String name){
		return symrepository.findRelated(name);
	}
	 public List<Disease> getDisease(String symname,String[] selected){
		 System.out.println("in get disease");
		return disrepository.findDisease(symname,selected);
	}
	 
	 public List<Disease> connectUser(String uname,String[] disease){
			return disrepository.conUserToDisease(uname,disease);
		}
     
	 public List<Symptom> addDataService(String sname,String frel,String fname,String inrel,String dname){
		 return symrepository.addAllData(sname, frel, fname, inrel, dname);
	 }
	 
	 public List<Disease> getSymDisease(String sname){
		 return disrepository.findSymDiseases(sname);
		 
	 }
}
