package com.telehealth365.SymptomChecker.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telehealth365.SymptomChecker.domain.User;
import com.telehealth365.SymptomChecker.repository.UserRepository;

@Service
public class RegisterService {
	
	@Autowired
	UserRepository<User> userrepository;

	public List<User> saveUser(String un,String pwd,String email,String age,String gender,String country){
		System.out.println("save user service");
		return userrepository.addUserNode(un, pwd, email, age, gender, country);
	}
	
	public List<User> validateUser(String un,String pwd){
		System.out.println("validate user service");
		return userrepository.validateNode(un, pwd);
		
	}

}
