package com.telehealth365.SymptomChecker.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.telehealth365.SymptomChecker.domain.AddData;
import com.telehealth365.SymptomChecker.domain.Disease;
import com.telehealth365.SymptomChecker.domain.Factor;
import com.telehealth365.SymptomChecker.domain.QuestionAnswer;
import com.telehealth365.SymptomChecker.domain.Symptom;
import com.telehealth365.SymptomChecker.message.Response;
import com.telehealth365.SymptomChecker.service.SymptomService;
import com.telehealth365.proto.GrpcClient;

@RestController
@RequestMapping(value = "/postsymptom")
public class SymptomController {
	@Autowired
	SymptomService symptomservice;
	
	
	@RequestMapping(value = "/getFactors", method = RequestMethod.POST)
	public Response postSymptom(@RequestBody QuestionAnswer qa) {
		// Create Response Object
		Response response = new Response();
		List<Factor> fres=symptomservice.getRelated(qa.getQuestion());
	    String uname=qa.getUname();
		
		if(fres.isEmpty()){
			response.setStatus("no symptom found");
		}
		else{
			response.setStatus("success");
			response.setSymname(qa.getQuestion());
			response.setFresponse(fres);
			response.setUname(uname);
		}
		
		return response;
  	}
	
	@RequestMapping(value = "/getDisease", method = RequestMethod.POST)
	public Response postRelated(@RequestBody QuestionAnswer qa) {
		// Create Response Object
		System.out.println("in postRelated()");
		Response response = new Response();
		String dname=null;
		
		String[] select=qa.getSelanswer();
		String[] unselect=qa.getUnselected();
		
		ArrayList<String> sal=new ArrayList<String>();
		for(int l=0;l<select.length;l++){
			sal.add(select[l]);
		}
		
		sal.add(qa.getQuestion());
		String[] pdata =new String[sal.size()];
		sal.toArray(pdata);
		
		System.out.println("---display pdata----");
		
		for(int n=0;n<pdata.length;n++){
			System.out.print(pdata[n]+" ");
		}
		
		System.out.println("---display 2d pred data----");
		String[][] pred=new String[][]{unselect,pdata};
		
		for(int p=0;p<pred.length;p++){
			for(int q=0;q<pred[p].length;q++){
				System.out.print(pred[p][q]+" ");
			}
			System.out.println();
		}
		
		
		String uname=qa.getUname();
		String symname=qa.getQuestion();
		//List<Disease> dis=symptomservice.getDisease(symname,select);
		
		List<Disease> symdis=symptomservice.getSymDisease(symname);
		String[] larray=new String[symdis.size()];
		String[][] farray=new String[symdis.size()][100];
		
		System.out.println("---display label set----");
		
		for(int i=0;i<symdis.size();i++){
			larray[i]=symdis.get(i).getName();
			
		}
		for(int k=0;k<larray.length;k++){
		System.out.print(larray[k]+"|  ");
		}
		 
		System.out.println();
		System.out.println();
		
		System.out.println("---display feature set----");
		
		for(int j=0;j<symdis.size();j++){
		for(int m=0;m<symdis.get(j).getSymarray().length;m++){
			farray[j][m]=symdis.get(j).getSymarray()[m];
			System.out.print(farray[j][m]+ " ");
		}
		System.out.println();
		}
		List<String> list = new ArrayList<String>();
		for(int r=0;r<farray.length;r++){
			
			 
		    for(String s : farray[r]) {
		       if(s != null && s.length() > 0) {
		          list.add(s);
		       } 
		    } 
		 
		    farray[r] = list.toArray(new String[list.size()]);    
		    list.clear();
		}
		 try{
		dname=GrpcClient.callGreet(farray,larray,pred);
		 }
		 catch(Exception e){
			 e.printStackTrace();
		 }
		/*String[] tempdis=new String[dis.size()];
		for(int i=0;i<dis.size();i++){
			tempdis[i]=dis.get(i).getName();
		}*/
		//List<Disease> disease=symptomservice.connectUser(uname, tempdis);		
		if(dname==null){
			response.setStatus("no Disease found");
		}
		else{
			response.setStatus("success");
			response.setDname(dname);
			response.setUname(uname);
		}
		
		return response;
  	}
	
	@RequestMapping(value = "/getData", method = RequestMethod.GET)
	public Response getData() {
		// Create Response Object
		System.out.println("in getData()");
		Response response = new Response();
		
		List<Disease> dis=symptomservice.findDiseases();
		List<Symptom> sym=symptomservice.findConnected();
		List<Factor> fact=symptomservice.findFactors();		
		if(dis.isEmpty() || sym.isEmpty() || fact.isEmpty() ){
			response.setStatus("no Data found");
		}
		else{
			response.setStatus("success");
			response.setDresponse(dis);
			response.setSymresponse(sym);
			response.setFresponse(fact);
		}
		
		return response;
  	}
	
	@RequestMapping(value = "/addData", method = RequestMethod.POST)
	public Response addData(@RequestBody AddData ad) {
		// Create Response Object
		System.out.println("in addData()");
		Response response = new Response();
		String sname=ad.getSymname();
		String fname=ad.getFactname();
		String dname=ad.getDisname();
		String frel=ad.getFactrel();
		String inrel=ad.getInrel();
		
		List<Symptom> sym=symptomservice.addDataService(sname, frel, fname, inrel, dname);
			
		if(sym.isEmpty()){
			response.setStatus("no Disease found");
		}
		else{
			response.setStatus("success");
			response.setSymresponse(sym);
		}
		
		return response;
  	}

}
