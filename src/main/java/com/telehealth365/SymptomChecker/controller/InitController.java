package com.telehealth365.SymptomChecker.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class InitController {
	
		@RequestMapping(value="/",method = RequestMethod.GET)
	    public String homepage(){
	        return "login";
	    }
		
		@RequestMapping(value="/admin",method = RequestMethod.GET)
	    public String adminPage(){
	        return "admin";
	    }

}
