package com.telehealth365.SymptomChecker.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.telehealth365.SymptomChecker.domain.Symptom;
import com.telehealth365.SymptomChecker.domain.User;
import com.telehealth365.SymptomChecker.message.Response;
import com.telehealth365.SymptomChecker.service.RegisterService;
import com.telehealth365.SymptomChecker.service.SymptomService;

@RestController
@RequestMapping(value = "/postuser")
public class RegisterController {
	//List<User> user = new ArrayList<User>();
	
	
	@Autowired
	RegisterService regservice;
	 
	@Autowired
	SymptomService symservice;

    @RequestMapping(value ="/adduser", method = RequestMethod.POST)
	public Response addUser(@RequestBody User user){
		// Create Response Object
		System.out.println("in user conroller");
		
		Response response=new Response();
		List<User> users=regservice.saveUser(user.getName(),user.getPassword(),user.getEmail(),user.getAge(),user.getGender(),user.getCountry());
		List<Symptom> syms =symservice.findConnected();
		if(users.isEmpty()){
			response.setStatus("no symptom found");
		}
		else{
	response.setStatus("success");
		response.setUname(users.get(0).getName());
		response.setSymresponse(syms);
		}
		
		return response;
  	}
		
}


