package com.telehealth365.SymptomChecker.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.telehealth365.SymptomChecker.domain.Symptom;
import com.telehealth365.SymptomChecker.domain.User;
import com.telehealth365.SymptomChecker.message.Response;
import com.telehealth365.SymptomChecker.service.RegisterService;
import com.telehealth365.SymptomChecker.service.SymptomService;

@RestController
@RequestMapping(value="/login")
public class LoginController {
	
	@Autowired
	RegisterService regservice;
	
	@Autowired
	SymptomService symservice;
	   
	@RequestMapping(value="/validate",method=RequestMethod.POST)
    public Response validate(@RequestBody User user){
		System.out.println("in login controller validate");
    	Response response=new Response();
    	String uname=user.getName();
    	System.out.println(uname);
    	String pwd=user.getPassword();
    	System.out.println(pwd);
    	List<User> users=regservice.validateUser(uname, pwd);
    	List<Symptom> syms =symservice.findConnected();
        if(users.isEmpty()){
        	response.setStatus("User not found");
        }
        else{
        	response.setSymresponse(syms);
        	response.setStatus("success");
    	response.setUname(users.get(0).getName());
        }
        return response;
    	
	}
	
	/*if(users.isEmpty()){
	System.out.println("isEmpty");
	return "redirect:/login/error";
}
else{
	System.out.println("is not empty");
	return "redirect:/login/symptom";
}*/
	
	
	
}
