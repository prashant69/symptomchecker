
package com.telehealth365.SymptomChecker.message;

import java.util.List;

import com.telehealth365.SymptomChecker.domain.Symptom;

public class UserResponse {
	
	private String status;
    private Long id;
    private String name;
	private String password;
	private String email;
	private String age;
	private String gender;
	private String country;
	
	private List<Symptom> symresponse;
	
	public List<Symptom> getSymresponse() {
		return symresponse;
	}

	public void setSymresponse(List<Symptom> symresponse) {
		this.symresponse = symresponse;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	
	
	public String getStatus() {
		return status;
	}
 
	public void setStatus(String status) {
		this.status = status;
	}

	

}
