package com.telehealth365.SymptomChecker.message;

import java.util.List;

import com.telehealth365.SymptomChecker.domain.Disease;
import com.telehealth365.SymptomChecker.domain.Factor;
import com.telehealth365.SymptomChecker.domain.Symptom;


public class Response {

	private String status;
	private String symname;
	private String uname;
	private String dname;
	
	private List<Symptom> symresponse;
	private List<Factor> fresponse;
	private List<Disease> dresponse;
	
	
	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}
	
	public String getDname() {
		return dname;
	}

	public void setDname(String dname) {
		this.dname = dname;
	}


	public String getSymname() {
		return symname;
	}

	public void setSymname(String symname) {
		this.symname = symname;
	}

	public List<Symptom> getSymresponse() {
		return symresponse;
	}
	public void setSymresponse(List<Symptom> symresponse) {
		this.symresponse = symresponse;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<Factor> getFresponse() {
		return fresponse;
	}
	public void setFresponse(List<Factor> fresponse) {
		this.fresponse = fresponse;
	}
	public List<Disease> getDresponse() {
		return dresponse;
	}
	public void setDresponse(List<Disease> dresponse) {
		this.dresponse = dresponse;
	}


}
