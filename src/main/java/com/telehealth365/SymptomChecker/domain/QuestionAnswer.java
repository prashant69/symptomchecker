package com.telehealth365.SymptomChecker.domain;

public class QuestionAnswer {
	private String question;
	private String[] selanswer;
	private String uname;
	private String[] diseases;
	private String[] unselected;
	
	public String[] getUnselected() {
		return unselected;
	}

	public void setUnselected(String[] unselected) {
		this.unselected = unselected;
	}

	public String[] getDiseases() {
		return diseases;
	}

	public void setDiseases(String[] diseases) {
		this.diseases = diseases;
	}

	public QuestionAnswer(){
		
	}
   
	public QuestionAnswer(String question,String[] selanswer,String uname,String[] diseases){
		this.question=question;
		this.selanswer=selanswer;
		this.uname=uname;
		this.diseases=diseases;
		
	}
	
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String[] getSelanswer() {
		return selanswer;
	}
	public void setSelanswer(String[] selanswer) {
		this.selanswer = selanswer;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	
	

}
