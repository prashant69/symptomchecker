package com.telehealth365.SymptomChecker.domain;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

@NodeEntity(label="symptom")
public class Symptom extends Factor {
	
	@GraphId
	private Long id;
	private String name;

	public void setId(Long id) {
		this.id = id;
	}
	public Symptom(){

	}
	public Symptom(String name){
		this.name=name;
		
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getId() {
		return id;
	}
	

}
