package com.telehealth365.SymptomChecker.domain;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity(label="dis")
public class Disease {

@GraphId
private Long id;	

private String name;

private String[] symarray;

public String[] getSymarray() {
	return symarray;
}
public void setSymarray(String[] symarray) {
	this.symarray = symarray;
}

private String type;

public String getType() {
	return type;
}
public void setType(String type) {
	this.type = type;
}

@Relationship(type="factor_of",direction=Relationship.INCOMING)
private List<Symptom> symptoms=new ArrayList<>();
	
	public Disease(){
		
	}
	public Disease(String name){
		this.name=name;
	}
	
	public List<Symptom> getSymptoms() {
		return symptoms;
	}
	public void setSymptoms(List<Symptom> symptoms) {
		this.symptoms = symptoms;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
