package com.telehealth365.SymptomChecker.domain;

public class AddData {
	private String symname;
	private String factrel;
	private String factname;
	private String inrel;
	private String disname;

public AddData(){
	
}

public String getSymname() {
	return symname;
}

public void setSymname(String symname) {
	this.symname = symname;
}

public String getFactrel() {
	return factrel;
}

public void setFactrel(String factrel) {
	this.factrel = factrel;
}

public String getFactname() {
	return factname;
}

public void setFactname(String factname) {
	this.factname = factname;
}

public String getInrel() {
	return inrel;
}

public void setInrel(String inrel) {
	this.inrel = inrel;
}

public String getDisname() {
	return disname;
}

public void setDisname(String disname) {
	this.disname = disname;
}


}
