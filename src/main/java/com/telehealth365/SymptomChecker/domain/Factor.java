package com.telehealth365.SymptomChecker.domain;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

@NodeEntity(label="factor")
public class Factor {

	@GraphId
	private Long id;
	
	public String getInrel() {
		return inrel;
	}

	public void setInrel(String inrel) {
		this.inrel = inrel;
	}

	public String getOutrel() {
		return outrel;
	}

	public void setOutrel(String outrel) {
		this.outrel = outrel;
	}

	private String name;
	private String inrel;
	private String outrel;
	public Factor(){

	}

	public Factor(Long id,String name){
		this.id=id;
		this.name=name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

}
