package com.telehealth365.SymptomChecker.repository;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

public interface SymptomRepository<Symptom> extends GraphRepository<Symptom> {
	
	@Query("Match(s:symptom)"+
			" return s ")
	public List<Symptom> findSymptoms();
	

	@Query("Match(s:symptom)-[r]->(f:symptom)  where s.name={name}"+
			" return f ")
	public List<Symptom> findRelated(@Param("name") String name);
	
	
	@Query("MERGE(s:symptom :factor {name:{sname},inrel:'accompanied by'})" +
			" MERGE (f:factor {name:{fname},inrel:{inrel}})" +
			" MERGE (d:dis {name:{dname}})" +
			" merge(f)-[r:factor_of]->(d)" +
			" merge(s)-[r2:factor_of]->(d)" +
			" WITH s,f,d" +
            " CALL apoc.merge.relationship(s,{frel}, {},{}, f) YIELD rel"+
            " RETURN s")
	public List<Symptom> addAllData(@Param("sname") String sname,@Param("frel") String frel,@Param("fname") String fname,
			@Param("inrel") String inrel,@Param("dname") String dname);
}
