package com.telehealth365.SymptomChecker.repository;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

public interface FactRepository<Factor> extends GraphRepository<Factor> {
	
	@Query("match(n:symptom{symname:{symname}})-[r:is]->(f:factor)"+
	"return f")
	public List<Factor> findrelFactors(@Param("symname")String symname);
	
	
	@Query("match(n:symptom{symname:{symname}})-[r:triggered_by]->(f:factor)"+
			"return f")
			public List<Factor> findTrigRelFactors(@Param("symname")String symname);
	
	
	@Query("Match(s:symptom)-[r]->(f:factor)  where s.name={name}"+
			" return f")
	public List<Factor> findRelated(@Param("name") String name);
	
	@Query("Match(f:factor)"+
			" return f")
	public List<Factor> findAllFactors();
	

}
