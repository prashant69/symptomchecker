package com.telehealth365.SymptomChecker.repository;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository<User> extends GraphRepository<User> {
	
	@Query("Merge(u:user{name:{name},password:{pwd},email:{email},age:{age},gender:{gender},country:{country}})"+
	" return u")
	public List<User> addUserNode(@Param("name") String name,@Param("pwd") String password,
			@Param("email") String email,@Param("age") String age,@Param("gender") String gender,
			@Param("country") String country);
	
	@Query("Match(u:user) where u.name={name} and u.password={pwd}" +
	" return u")
	public List<User> validateNode(@Param("name") String name,@Param("pwd") String password);
	
}
