package com.telehealth365.SymptomChecker.repository;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

public interface DiseaseRepository<Disease> extends GraphRepository<Disease> {
	
	@Query("Match(s:symptom)-[r]->(f:factor)-[r2:factor_of]->(d:dis)<-[r3:factor_of]-(s)"+
	"where s.name={symname} and f.name in{selected}  with d,count(r2) as rel_cnt WHERE rel_cnt >= 2"+
			" return d")
    public List<Disease> findDisease(@Param("symname")String symname,@Param("selected") String[] selected);
	
	@Query("match(u:user) match(d:dis) where u.name={uname} and d.name in {disease} merge(u)-[:suffering_from]->(d)"+
			" return d")
	 public List<Disease> conUserToDisease(@Param("uname") String uname,@Param("disease")String[] disease);
	
	
	@Query("Match(d:dis)"+
			" return d")
	public List<Disease> findAllDiseases();
	
	@Query("Match(s:symptom)-[r]->(f:factor)-[r2:factor_of]->(d:dis)<-[r3:factor_of]-(s)"+
			"where s.name={symname}"+
					" return d")
	public List<Disease> findSymDiseases(@Param("symname")String symname);

}
