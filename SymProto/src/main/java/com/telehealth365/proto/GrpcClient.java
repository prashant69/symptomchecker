package com.telehealth365.proto;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.telehealth365.proto.generated.ArrayRequest;
import com.telehealth365.proto.generated.GreeterGrpc;
import com.telehealth365.proto.generated.Pythonml;
import com.telehealth365.proto.generated.Selected;
import com.telehealth365.proto.generated.StringReply;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

public class GrpcClient {
	private static final Logger logger = Logger.getLogger(GrpcClient.class.getName());

	private final ManagedChannel channel;
	private final GreeterGrpc.GreeterBlockingStub blockingStub;


	/** Construct client connecting to HelloWorld server at {@code host:port}. */
	public GrpcClient(String host, int port) {
		this(ManagedChannelBuilder.forAddress(host, port)
				// Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
				// needing certificates.
				.usePlaintext(true)
				.build());
	}

	/** Construct client for accessing RouteGuide server using the existing channel. */
	GrpcClient(ManagedChannel channel) {
		this.channel = channel;
		blockingStub = GreeterGrpc.newBlockingStub(channel);
	}

	public void shutdown() throws InterruptedException {
		channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
	}

	/** Say hello to server. */
	public String greet(String[][] features,String[] label,String[][] sdata) {
		// logger.info("Will try to Send Data " + feature + " ...");
		System.out.println("----in grpc greet()---");

		ArrayList<String> lb = new ArrayList<String>();
		for(int k=0;k<label.length;k++){
			lb.add(label[k]); 
		}

		ArrayList<Pythonml> pal=new ArrayList<Pythonml>();
		ArrayList<Selected> sel=new ArrayList<Selected>();

		ArrayList<String> fdl=new ArrayList<String>();
		ArrayList<String> pd=new ArrayList<String>();

		for (int i=0;i<features.length;i++){
			for(int j=0;j<features[i].length;j++){
				fdl.add(j,features[i][j]);
			}
			pal.add(i,Pythonml.newBuilder().addAllFdata(fdl).build());
			fdl.clear();
		}

		for (int n=0;n<pal.size();n++){
			System.out.println(pal.get(n).getFdataList()+ " ");
		}

		for (int l=0;l<sdata.length;l++){
			for(int m=0;m<sdata[l].length;m++){
				pd.add(m,sdata[l][m]);
			}
			sel.add(l,Selected.newBuilder().addAllPdata(pd).build());
			pd.clear();
		}

		ArrayRequest request=ArrayRequest.newBuilder().addAllLabel(lb).addAllFeature(pal).addAllData(sel).build();
		String reply=null;
		StringReply response=null;
		try {
			response = blockingStub.sendFeatureLabel(request);
		} catch (StatusRuntimeException e) {
			logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
			return "error";
		}
		logger.info("Greeting: " + response.getPredict());
		reply=response.getPredict();
		return reply;
	}

	/**
	 * Greet server. If provided, the first element of {@code args} is the name to use in the
	 * greeting.
	 */
	// public static void main(String[] args) throws Exception {
	public static String callGreet(String[][] feat,String[] lbl,String[][] sd) throws Exception{
		GrpcClient client = new GrpcClient("localhost", 50051);
		String pred=null;
		System.out.println("----in grpc callGreet()---");
		for(int i=0;i<feat.length;i++){
			for(int j=0;j<feat[i].length;j++){
				System.out.print(feat[i][j]+"  ");
			}
			System.out.println();
		}
		try {
			/* Access a service running on the local machine on port 50051 */
			//String user = "world";
			/*if (args.length > 0) {
		        user = args[0];  Use the arg as the name to greet if provided 
		      }*/
			pred = client.greet(feat,lbl,sd);
			//return pred;
		} finally {
			client.shutdown();
		}
		return pred;
	}


}
